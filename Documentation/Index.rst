﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: Includes.txt

.. _start:

=============================================================
SMS Login TYPO3 Extension for Two Factor Authentication
=============================================================

.. only:: html

	:Classification:
		smslogin

	:Version:
		|release|

	:Language:
		en

	:Description:
		'smslogin' extension provides two-way authentication for TYPO3 websites. By Default GTX SMS Gateway is integrated with this plugin. With the use of integrated Signal Slot Dispatcher developer could able to extend the OTP functionality with any other sms gateways. Plugin also handles forgot password form. Sponsored By Abteilung für Gestaltung [https://abteilung.ch/]

	:Keywords:
		smslogin,feuser,login,otplogin,smslogin,twofactorsecurity

	:Copyright:
		2016

	:Author:
		HOJA.M.A

	:Email:
		hoja.ma@pitsolutions.com

	:License:
		This document is published under the Open Content License
		available from http://www.opencontent.org/opl.shtml

	:Rendered:
		|today|

	The content of this document is related to TYPO3,
	a GNU/GPL CMS/Framework available from `www.typo3.org <http://www.typo3.org/>`_.

	**Table of Contents**

.. toctree::
	:maxdepth: 3
	:titlesonly:

	Introduction/Index
	User/Index
	Configuration/Index
	Developer/Index
	ChangeLog/Index
	KnownProblems/Index

