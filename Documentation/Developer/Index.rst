﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _developer:

Developer Corner
================


The extension provide a SignalSlot for extending.


.. _developer-signals:


SignalSlots
-----------

beforeSendSMS


Input parameters are:

+-----------------------+---------------+---------------------------------+
| Parameter             | Data type     | Description                     |
+=======================+===============+=================================+
| $this->extensionName  | string        | Name of the extension           |
+-----------------------+---------------+---------------------------------+
| $this                 | object        | The current object              |
+-----------------------+---------------+---------------------------------+
| &$result_status       | boolean       | Result status                   |
+-----------------------+---------------+---------------------------------+


How to use the Signal...
Write the code below in your ext_localconf.php file

.. code-block:: php

	<?php
	/** @var \TYPO3\CMS\Extbase\SignalSlot\Dispatcher $signalSlotDispatcher */
	$signalSlotDispatcher = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
	\TYPO3\CMS\Extbase\SignalSlot\Dispatcher::class);

	$signalSlotDispatcher->connect(
	'PITS\Smslogin\Controller',
	'beforeSendSMS',
	'Your\Extension\Code\Path',
	'funcitonName',
	FALSE
	);
