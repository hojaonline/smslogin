﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _configuration:

Configuration
=============

* Install and activate the extension using extension manager.
* Fill the settings fields to activate SMS OTP functionality

Extension settings
-------------------
1. SMS Login API Key - Enter the API key which you get after registering in ..
2. SMS Login API Secret/Pass - Enter here the API secret value.


.. figure:: ../Images/UserManual/conf.png
   :width: 500px
   :alt: be1

    Backend extension settings


Adding the Plugin
-----------------

* Add the extention in the inlcude static template section of the template record.
* Add the plugin to a page as a content.
* Fill in the plugin configurations:



Plugin Options
::::::::::::::

General
--------
* Enable OTP through User Email :- Check this to send OTP to user Email.
* Show forgot password link :- Enable to display forgot password link.
* Display Logout Form After Successful Login :- Enable to show the logout form after login.
* Select User Group For FE USer:- Select the usergroup here
* User Storage Page :- select the folder where the user records are stored.


.. figure:: ../Images/UserManual/BackendView.png
   :width: 600px
   :alt: be1

    Backend plugin configurations


Redirects
----------
* After Successful login redirect to page :- Select the corresponding page to be redirected to after successful login.

* After failed login redirect to page :- Select the corresponding page to be redirected to after failed login.


.. figure:: ../Images/UserManual/beView2.png
   :width: 600px
   :alt: be2

    Backend plugin configurations



.. toctree::
	:maxdepth: 3
	:titlesonly:

	Typoscript/Index

