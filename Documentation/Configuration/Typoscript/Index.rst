.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _configuration:

TypoScript
::::::::::

The extension has various typoscript settings which need to be filled for successful fucntioning of the plugin.

setup.ts
--------


.. code-block:: setup.ts

	plugin.tx_smslogin{
	view {
		templateRootPath = {$plugin.tx_smslogin.view.templateRootPath}
		partialRootPath = {$plugin.tx_smslogin.view.partialRootPath}
		layoutRootPath = {$plugin.tx_smslogin.view.layoutRootPath}
	}
	persistence {
		storagePid = {$plugin.tx_smslogin.persistence.storagePid}
	}

	settings {
		# time in hours how long the link for forget password is valid
		forgotLinkHashValidTime = 12

		dateFormat = Y-m-d H:i

		# baseURL for the link generation
		feloginBaseURL =

		# Mail settings
		email_from =
		email_fromName =
		replyTo =

		# Expose the information on whether or not the account for which a new password was requested exists. By default, that information is not disclosed for privacy reasons.
		exposeNonexistentUserInForgotPasswordDialog = 0

		# when setting a new password, this is the minimum amount of characters
		newPasswordMinLength = 6
	}


Details:

+---------------------------------+------------------------------------------+-------------+
| Parameter                       | Description  -                           | Default     |
+=================================+==========================================+=============+
| forgotLinkHashValidTime         | valid time for hash in hours             | 12          |
+---------------------------------+------------------------------------------+-------------+
| dateFormat                      | format of the date                       | Y-m-d H:i   |
+---------------------------------+------------------------------------------+-------------+
| feloginBaseURL                  | baseURL for link generation              |             |
+---------------------------------+------------------------------------------+-------------+
| email_from                      | Admin email                              |             |
+---------------------------------+------------------------------------------+-------------+
| email_fromName                  | Admin name                               |             |
+---------------------------------+------------------------------------------+-------------+
| replyTo                         | Reply email id                           |             |
+---------------------------------+------------------------------------------+-------------+
| newPasswordMinLength            | Minimum length of password               | 6           |
+---------------------------------+------------------------------------------+-------------+


