
plugin.tx_smslogin{
    view {
        # cat=plugin.tx_smslogin/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:smslogin/Resources/Private/Templates/
        # cat=plugin.tx_clockwork/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:smslogin/Resources/Private/Partials/
        # cat=plugin.tx_smslogin/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:smslogin/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_smslogin//a; type=string; label=Default storage PID
        storagePid =
    }
}
