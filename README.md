# SMS Login TYPO3 Extension for Two Factor Authentication #

'smslogin' extension provides two-way authentication for TYPO3 websites. By Default GTX SMS Gateway is integrated with this plugin. With the use of integrated Signal Slot Dispatcher developer could able to extend the OTP functionality with any other sms gateways. Plugin also handles forgot password form. Sponsored By Abteilung für Gestaltung [https://abteilung.ch/]

Keywords: smslogin,feuser,login,otplogin,smslogin,twofactorsecurity

Author: HOJA.M.A
Email: hoja.ma@pitsolutions.com
