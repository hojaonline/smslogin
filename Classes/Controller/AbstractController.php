<?php
namespace PITS\Smslogin\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 HOJA.M.A <hoja.ma@pitsolutions.com>, PIT Solutions PVT LTD
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Saltedpasswords\Salt\SaltFactory;

/**
 * AbstractController
 */
abstract class AbstractController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {
    /**
     * @var \DateTime The current time
     */
    protected $dateTime;

    /**
     * @var array
     */
    protected $extConf = array();

    /**
     * contains the ts settings for the current action
     *
     * @var array
     */
    protected $actionSettings = array();

    /**
     * application context of typo3
     * @var string
     */
    protected $application_context;

    /**
     * contains the specific ts settings for the current controller
     *
     * @var array
     */
    protected $controllerSettings = array();

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager
     * @inject
     */
    protected $persistenceManager = NULL;

    /**
     * @var \PITS\Smslogin\Services\MailUtility
     * @inject
     */
    protected $emailUtility;

    /**
     * @var int
     */
    protected $currentPageUid;

    /**
     * @var string
     * extensionName
     */
    protected $extensionName;

    /**
     * frontendUserGroupRepository
     * @var \TYPO3\CMS\Extbase\Domain\Repository\FrontendUserGroupRepository
     * @inject
     */
    protected $frontendUserGroupRepository = NULL;

    /**
     * $cObj
     * @var \TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer
     * @inject
     */
    protected $cObj = NULL;

    /**
     * objectManager
     * @var \TYPO3\CMS\Extbase\Object\ObjectManagerInterface
     */
    protected $objectManager = NULL;

    /**
     * userRepository
     * @var PITS\Smslogin\Domain\Repository\FrontendUserRepository
     * @inject
     */
    protected $userRepository = NULL;

    /**
     * sessionHandler Service
     * @var PITS\Smslogin\Services\SessionHandler
     * @inject
     */
    protected $sessionHandler;

    /**
     * @var int
     */
    protected $userStoragePid = 0;

    /**
     * @var
     */
    protected $gtxAuthenticationLibrary;

    /**
     * @var string
     */
    protected $isLogin = NULL;

    /**
     * frontendUser $feUser
     * @var \PITS\Smslogin\Domain\Model\FrontendUser
     * @inject
     */
    protected $feUser = NULL;

    /**
     * $userGroupSelected
     * Selected Groups in FlexForm
     */
    protected $userGroupSelected;

    /**
     * Initializes the controller before invoking an action method.
     *
     * Override this method to solve tasks which all actions have in
     * common.
     *
     * @return void
     */
    protected function initializeAction() 
    {
        parent::initializeAction();
        $this->userStoragePid = isset( $this->settings['storagePid'] )?$this->settings['storagePid']:0;
        $this->userGroupSelected = isset( $this->settings['usergroup'] )?$this->settings['usergroup']:FALSE;
        $this->extensionName = $this->request->getControllerExtensionName();
        $this->dateTime = new \DateTime('now', new \DateTimeZone('Europe/Berlin'));
        $this->extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][GeneralUtility::camelCaseToLowerCaseUnderscored($this->extensionName)]);
        $this->gtxAuthenticationLibrary = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('PITS\Smslogin\Packages\GTX\GTXMessaging',
            $this->extConf );
        $this->controllerSettings = $this->settings['controllers'][$this->request->getControllerName()]; 
        $this->actionSettings = $this->controllerSettings['actions'][$this->request->getControllerActionName()];
        $this->currentPageUid = $GLOBALS['TSFE']->id;
        $configurationManager = $this->objectManager->get('TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface');
        $this->frontendUserGroupRepository = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Domain\Repository\\FrontendUserGroupRepository');
        /** @var $querySettings \TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings */
        $querySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
        // go for $defaultQuerySettings = $this->createQuery()->getQuerySettings(); if you want to make use of the TS persistence.storagePid with defaultQuerySettings(), see #51529 for details
        // don't add the pid constraint
        $querySettings->setRespectStoragePage(FALSE);
        // set the storagePids to respect
        $querySettings->setStoragePageIds(array(30));
        $currentApplicationContext = \TYPO3\CMS\Core\Utility\GeneralUtility::getApplicationContext();
        $this->application_context = ($currentApplicationContext->isProduction())?"PRODUCTION":"DEVELOPMENT";
        $this->frontendUserGroupRepository->setDefaultQuerySettings ($querySettings);
        $this->isLogin = ($GLOBALS['TSFE']->fe_user->user )?1:0;
        $this->feUser = ($this->isLogin)? $this->userRepository->findByUid( $GLOBALS['TSFE']->fe_user->user['uid']) :NULL;
        if (($this->request->hasArgument('action') && 
            $this->request->getArgument('action') != "success" && 
            $this->isLogin == 1 ) ){
            $this->redirect('success');
        } elseif ( empty( $this->request->getArguments() ) && $this->isLogin == 1 ) {
            $this->redirect('success');   
        }
    }

    /**
     * Initializes the view before invoking an action method.
     *
     * Override this method to solve assign variables common for all actions
     * or prepare the view in another way before the action is called.
     *
     * @param \TYPO3\CMS\Extbase\Mvc\View\ViewInterface $view
     * @return void
     */
    protected function initializeView( \TYPO3\CMS\Extbase\Mvc\View\ViewInterface $view ) 
    {
        parent::initializeView($view);
        $this->view->assignMultiple(array(
            'controllerSettings' => $this->controllerSettings,
            'actionSettings' => $this->actionSettings,
            'extConf' => $this->extConf,
            'currentPageUid' => $this->currentPageUid,
            'dateTime' => $this->dateTime
        ));
    }

    /**
     * redirects to page
     *
     * @param null $pageUid
     * @param array $additionalParams
     * @param int $pageType
     * @param bool $noCache
     * @param bool $noCacheHash
     * @param string $section
     * @param bool $linkAccessRestrictedPages
     * @param bool $absolute
     * @param bool $addQueryString
     * @param array $argumentsToBeExcludedFromQueryString
     */
    protected function redirectToPage(
        $pageUid = NULL, 
        array $additionalParams = array(), 
        $pageType = 0, 
        $noCache = FALSE, 
        $noCacheHash = FALSE, 
        $section = '', 
        $linkAccessRestrictedPages = FALSE, 
        $absolute = TRUE, 
        $addQueryString = FALSE, 
        array $argumentsToBeExcludedFromQueryString = array()
    ) {
        $uri = $this->uriBuilder
            ->reset()
            ->setTargetPageUid($pageUid)
            ->setTargetPageType($pageType)
            ->setNoCache($noCache)
            ->setUseCacheHash(!$noCacheHash)
            ->setSection($section)
            ->setLinkAccessRestrictedPages($linkAccessRestrictedPages)
            ->setArguments($additionalParams)
            ->setCreateAbsoluteUri($absolute)
            ->setAddQueryString($addQueryString)
            ->setArgumentsToBeExcludedFromQueryString($argumentsToBeExcludedFromQueryString)
            ->build();
        $this->redirectToURI($uri);
    }

    /**
     * Deactivate FlashMessages for errors
     * @see TYPO3\CMS\Extbase\Mvc\Controller\ActionController::getErrorFlashMessage()
     */
    protected function getErrorFlashMessage() {
        return FALSE;
    }

    /**
     * Returns the current view
     *
     * @return \TYPO3\CMS\Extbase\Mvc\View\ViewInterface
     */
    public function getView() {
        return $this->view;
    }

    /**
     * Setter for password. As we have encrypted passwords here, we need to encrypt them before storing!
     * @param $password
     * @return string
     */
    public function setPassword($password) {
        if (ExtensionManagementUtility::isLoaded('saltedpasswords')) {
            $saltedpasswordsInstance = SaltFactory::getSaltingInstance();
            $encryptedPassword = $saltedpasswordsInstance->getHashedPassword($password);
            $password = $encryptedPassword;
            return $password;
        } else {
            return $password;
        }
    }

    /**
     * @param $username
     * @param $password
     * @return bool
     */
    public function validateFrontEndUser($username , $password ){
        $frontendUser = $this->userRepository->findOneByUsernameEntered( 
            $username , 
            $this->userStoragePid , 
            $this->userGroupSelected 
        );
        if( $frontendUser instanceof \PITS\Smslogin\Domain\Model\FrontendUser ){
            $user_password = $frontendUser->getPassword();
            $status = $this->userRepository->checkPassword( $password , $user_password );
            if( !status ){
                $this->addFlashMessage(
                   $this->translate('password_incorrect_msg'),
                   $messageTitle = '',
                   $severity = \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR,
                   $storeInSession = TRUE
                );
            }
        } else {
            $this->addFlashMessage(
               $this->translate('username_incorrect_msg'),
               $messageTitle = '',
               $severity = \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR,
               $storeInSession = TRUE
            );
            $status = TRUE;
        }
        return $status;        
    }

    /**
     * @param null $pageUid
     * @param array $additionalParams
     * @param int $pageType
     * @param bool|FALSE $noCache
     * @param bool|FALSE $noCacheHash
     * @param string $section
     * @param bool|FALSE $linkAccessRestrictedPages
     * @param bool|FALSE $absolute
     * @param bool|FALSE $addQueryString
     * @param array $argumentsToBeExcludedFromQueryString
     * @return string
     */
    public function buildUri(
        $pageUid = NULL, 
        array $additionalParams = array(), 
        $pageType = 0, 
        $noCache = FALSE, 
        $noCacheHash = FALSE, 
        $section = '', 
        $linkAccessRestrictedPages = FALSE, 
        $absolute = FALSE, 
        $addQueryString = FALSE, 
        array $argumentsToBeExcludedFromQueryString = array() 
    ){
        return $uri = $this->uriBuilder
            ->reset()
            ->setTargetPageUid($pageUid)
            ->setTargetPageType($pageType)
            ->setNoCache($noCache)
            ->setUseCacheHash(!$noCacheHash)
            ->setSection($section)
            ->setLinkAccessRestrictedPages($linkAccessRestrictedPages)
            ->setArguments($additionalParams)
            ->setCreateAbsoluteUri($absolute)
            ->setAddQueryString($addQueryString)
            ->setArgumentsToBeExcludedFromQueryString($argumentsToBeExcludedFromQueryString)
            ->build();
    }

    /**
     * @param $key
     * @return NULL|string
     */
    public function translate($key ) 
    {
        return \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate(
            $key, 
            $this->extensionName,
            $arguments = null
        );
    }
}