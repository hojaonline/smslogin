<?php
namespace PITS\Smslogin\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 HOJA.M.A <hoja.ma@pitsolutions.com>, PIT Solutions PVT LTD
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Saltedpasswords\Salt\SaltFactory;

/**
 * LoginController
 */
class LoginController extends AbstractController {
    
    /**
     * prefix id of extension
     * @var string
     */
    public $prefixId = 'tx_smslogin_smsfelogin';
    
    /**
     * action login
     *
     * @return void
     */
    public function loginAction() 
    {
        if( $this->request->hasArgument('submit') ) {
            $username = ( $this->request->hasArgument('username') )?$this->request->getArgument('username'):NULL;
            $password = ( $this->request->hasArgument('password') )?$this->request->getArgument('password'):NULL;
            if( !empty( $username ) && !empty( $password ) ){
                $validation_status = $this->validateFrontEndUser( $username , $password );
                if( $validation_status ){
                    $this->forward( 'confirmotp' , NULL , NULL , $this->request->getArguments() );
                }
                else{
                    $this->addFlashMessage(
                       $this->translate('empty_username_or_password'),
                       $messageTitle = '',
                       $severity = \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR,
                       $storeInSession = TRUE
                    );
                }
            }
            else {
                $this->addFlashMessage(
                   $this->translate('empty_username_or_password'),
                   $messageTitle = '',
                   $severity = \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR,
                   $storeInSession = TRUE
                );
            }
            $redirect_after_loginerror = ( isset($this->settings['redirectPageLoginError']) && $this->settings['redirectPageLoginError'] > 0 ) ? $this->settings['redirectPageLoginError'] :FALSE;
            if( $redirect_after_loginerror ) {
                $this->redirectToPage( $redirect_after_loginerror );
            }
        }
    }
    
    /**
     * action confirmotp
     *
     * @return void
     */
    public function confirmotpAction() 
    {
        if ( $this->request->hasArgument('otpsubmit')) {
            $user = $this->sessionHandler->restoreFromSession('user_validate_user');
            if( $user instanceof \PITS\Smslogin\Domain\Model\FrontendUser ){
                $OTP = $this->sessionHandler->restoreFromSession('user_validate_otp'.$user->getUid());
                $user_otp = $this->request->getArgument('otp');
                if( $OTP == $user_otp ){
                    $this->loginUser( $user->getUsername(),$user->getPassword() );
                }
                else{
                    $this->addFlashMessage(
                       $this->translate('otp_incorrect'), 
                       $messageTitle = '',
                       $severity = \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR,
                       $storeInSession = TRUE
                    );
                    if( $user->getTelephone() != NULL ){
                        $sms_message_template = $this->translate('otp_message_template');
                        $OTP = $this->gtxAuthenticationLibrary->generateOTP();
                        //echo PHP_EOL."OTP is :".$OTP." enabled for production in ".__FILE__." line number ".__LINE__.PHP_EOL;
                        $OTP_ID = $this->gtxAuthenticationLibrary->generateOTP_ID();
                        $this->sessionHandler->writeToSession('user_validate_user', $user );
                        $this->sessionHandler->writeToSession('user_validate_otp'.$user->getUid(),$OTP);
                        $message = vsprintf( $sms_message_template , array('0'=>$OTP,'1'=> $OTP_ID));
                        /** @var \TYPO3\CMS\Extbase\SignalSlot\Dispatcher $signalSlotDispatcher */
                        $signalSlotDispatcher = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\SignalSlot\Dispatcher::class);
                        //DebuggerUtility::var_dump( $signalSlotDispatcher );exit;
                        $result_status = true;
                        $signalSlotDispatcher->dispatch(__CLASS__, 'beforeSendSMS', array($this->extensionName, $this , &$result_status ));
                        //DebuggerUtility::var_dump( $result_status );exit;
                        if( $result_status ){
                            $this->gtxAuthenticationLibrary->sendSMS(
                                $from = $this->translate('sms_sender_name'),
                                $to = $user->getTelephone(),
                                $message
                            );
                        }
                        $this->view->assign('otp_id',$OTP_ID);
                    }
                }
            }           
        }
        else{
            $user = $this->userRepository->findOneByUsernameEntered( $this->request->getArgument('username') , $this->userStoragePid );
            if( $user instanceof \PITS\Smslogin\Domain\Model\FrontendUser ){
                if( $user->getTelephone() != NULL ){
                    $sms_message_template = $this->translate('otp_message_template');
                    $OTP = $this->gtxAuthenticationLibrary->generateOTP();
                    //echo PHP_EOL."OTP is :".$OTP." enabled for production in ".__FILE__." line number ".__LINE__.PHP_EOL;
                    $OTP_ID = $this->gtxAuthenticationLibrary->generateOTP_ID();
                    $user->setTxSmsloginOtptoken( $OTP );
                    $this->userRepository->update( $user );
                    $this->userRepository->persistAll();
                    $this->sessionHandler->writeToSession('user_validate_user', $user );
                    $this->sessionHandler->writeToSession('user_validate_otp'.$user->getUid(),$OTP);
                    $message = vsprintf( $sms_message_template , array('0'=>$OTP,'1'=> $OTP_ID));
                    $this->gtxAuthenticationLibrary->sendSMS( 
                        $from = $this->translate('sms_sender_name'),
                        $to = $user->getTelephone(),
                        $message 
                    );
                    if( $this->settings['enableMailOTP'] ){
                        $this->sendOtpEmail( $user , $OTP , $OTP_ID );
                    }
                    $this->view->assign('otp_id',$OTP_ID);
                }
                else{
                    $this->loginUser( $user->getUsername() , $user->getPassword() );
                }
            }
            else{
                $this->redirect('login');
            }
        }
    }
    
    /**
     * action forgotpassword
     *
     * @return void
     */
    public function forgotpasswordAction()
    {
        if( $this->request->hasArgument('forgotpassword') && $this->request->hasArgument('username') ){
            //$hash = $this->sessionHandler->restoreFromSession('forgot_hash');
            $pid = $this->settings['storagePid'];
            // Get hashes for compare
            $postedHash = $this->request->getArgument('forgot_hash');
            $hashData = $this->sessionHandler->restoreFromSession('forgot_hash');
            if ( $postedHash === $hashData ) {
                $row = false;
                // Look for user record
                $this->databaseConnection =  $GLOBALS['TYPO3_DB'];
                $data = $GLOBALS['TYPO3_DB']->fullQuoteStr( $this->request->getArgument('username'), 'fe_users');
                $res = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
                    'uid, username, password, email',
                    'fe_users',
                    '(email=' . $data . ' OR username=' . $data . ') AND pid IN (' . $this->databaseConnection->cleanIntList($pid) . ') ' . $this->cObj->enableFields('fe_users')
                );

                if ($this->databaseConnection->sql_num_rows($res)) {
                    $row = $this->databaseConnection->sql_fetch_assoc($res);
                }
                //DebuggerUtility::var_dump( $row );
                $error = null;
                if ($row) {
                    // Generate an email with the hashed link
                    $error = $this->generateAndSendHash($row);

                } elseif ($this->settings['exposeNonexistentUserInForgotPasswordDialog']) {
                    $error = $this->translate('ll_forgot_reset_message_error');
                }

                // Generate message
                if ($error) {
                    $msgArray['STATUS_MESSAGE'] = $this->cObj->stdWrap($error, $this->settings['forgotErrorMessage_stdWrap.']);
                } else {
                    $msgArray['STATUS_MESSAGE'] = $this->cObj->stdWrap(
                        $this->translate('ll_forgot_reset_message_emailSent'),
                        $this->settings['forgotResetMessageEmailSentMessage_stdWrap.']
                    );
                    $this->view->assign('msgArray',$msgArray);
                }

                $subpartArray['###FORGOT_FORM###'] = '';
            } else {
                // Wrong email
                //$markerArray['###STATUS_MESSAGE###'] = $this->getDisplayText('forgot_reset_message', $this->conf['forgotMessage_stdWrap.']);
                //$markerArray['###BACKLINK_LOGIN###'] = '';
            }

            //exit();
        }
        // Generate hash
        $hash = md5($this->generatePassword(3));
        // Set hash in feuser session
        $this->sessionHandler->writeToSession('forgot_hash', $hash );
        $this->view->assign('forgot_hash',$hash);
    }

    /**
     * action success
     *
     * @return void
     */
    public function successAction() 
    {
        if( $this->isLogin ){
            $this->view->assign('user',$this->feUser);
        }
        else{
            $this->redirect('login');
        }
    }

    /**
     * loginUser
     * @param $username
     * @param $password
     */
    public function loginUser($username, $password)
    {
        $GLOBALS['TSFE']->fe_user->checkPid = '';
        $redirect_after_login = ( isset($this->settings['redirectPageLogin']) && $this->settings['redirectPageLogin'] > 0 ) ? $this->settings['redirectPageLogin'] :FALSE;
        $info = $GLOBALS['TSFE']->fe_user->getAuthInfoArray();
        $user = $GLOBALS['TSFE']->fe_user->fetchUserRecord($info['db_user'], $username);
        $tsfe = $GLOBALS['TSFE'];
        $tsfe->fe_user->createUserSession($user);
        // enforce session so we get a FE cookie, otherwise autologin does not work (TYPO3 6.2.5+)
        $tsfe->fe_user->setAndSaveSessionData('dummy', TRUE);
        if( $redirect_after_login ){
            $this->redirectToPage( $redirect_after_login );
        }
        $this->redirect('success');
    }

    /**
     * Is used by forgot password - function with md5 option.
     * @param int $len Length of new password
     * @return string New password
     */
    protected function generatePassword($len)
    {
        $pass = '';
        while ($len--) {
            $char = rand(0, 35);
            if ($char < 10) {
                $pass .= '' . $char;
            } else {
                $pass .= chr($char - 10 + 97);
            }
        }
        return $pass;
    }

    /**
     * Generates a hashed link and send it with email
     *
     * @param array $user Contains user data
     * @return string Empty string with success, error message with no success
     */
    protected function generateAndSendHash($user)
    {
        //$hours = (int)$this->conf['forgotLinkHashValidTime'] > 0 ? (int)$this->conf['forgotLinkHashValidTime'] : 24;
        $hours = (int)$this->settings['forgotLinkHashValidTime'] > 0 ? (int)$this->settings['forgotLinkHashValidTime'] : 24;
        $validEnd = time() + 3600 * $hours;
        //$validEndString = date($this->conf['dateFormat'], $validEnd);
        $validEndString = date($this->settings['dateFormat'], $validEnd);
        $hash = md5(GeneralUtility::generateRandomBytes(64));
        $randHash = $validEnd . '|' . $hash;
        $randHashDB = $validEnd . '|' . md5($hash);
        // Write hash to DB
        $res = $this->databaseConnection->exec_UPDATEquery('fe_users', 'uid=' . $user['uid'], array('felogin_forgotHash' => $randHashDB));

        // Send hashlink to user
        $this->conf['linkPrefix'] = -1;
        //$isAbsRefPrefix = !empty($this->frontendController->absRefPrefix);
        $isAbsRefPrefix = $GLOBALS['TSFE']->absRefPrefix;
       // $isBaseURL = !empty($this->frontendController->baseUrl);
        $isBaseURL = $GLOBALS['TSFE']->baseUrl;
        $isFeloginBaseURL = !empty($this->settings['feloginBaseURL']);

        $link = $this->buildUri(
            //$this->frontendController->id, 
            $GLOBALS['TSFE'] ->id,
            array(
                rawurlencode($this->prefixId . '[action]') => 'changepassword',
                rawurlencode($this->prefixId . '[user]') => $user['uid'],
                rawurlencode($this->prefixId . '[forgothash]') => $randHash
            )
        );
        
        // Prefix link if necessary
        if ($isFeloginBaseURL) {
            // First priority, use specific base URL
            // "absRefPrefix" must be removed first, otherwise URL will be prepended twice
            if ($isAbsRefPrefix) {
                $link = substr($link, strlen($GLOBALS['TSFE']->absRefPrefix));
            }
            $link = $this->settings['feloginBaseURL'] . $link;
        } elseif ($isAbsRefPrefix) {
            // Second priority
            // absRefPrefix must not necessarily contain a hostname and URL scheme, so add it if needed
            $link = GeneralUtility::locationHeaderUrl($link);
        } elseif ($isBaseURL) {
            // Third priority
            // Add the global base URL to the link
            $link = $GLOBALS['TSFE']->baseUrlWrap($link);
        } else {
            // No prefix is set, return the error
            return $this->translate('ll_change_password_nolinkprefix_message');
        }

        $msg = sprintf($this->translate('ll_forgot_validate_reset_password'), $user['username'], $link, $validEndString);
        //DebuggerUtility::var_dump($msg);
        // Add hook for extra processing of mail message
        if (
            isset($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['felogin']['forgotPasswordMail'])
            && is_array($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['felogin']['forgotPasswordMail'])
        ) {
            $params = array(
                'message' => &$msg,
                'user' => &$user
            );
            foreach ($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['felogin']['forgotPasswordMail'] as $reference) {
                if ($reference) {
                    GeneralUtility::callUserFunction($reference, $params, $this);
                }
            }
        }
        if ($user['email']) {
            $this->cObj->sendNotifyEmail($msg, $user['email'], '', $this->settings['email_from'], $this->settings['email_fromName'], $this->settings['replyTo']);
        }
        return '';
    }

    /**
     * @param $user
     * @param $OTP
     * @param $OTP_ID
     * @return bool
     */
    public function sendOtpEmail($user , $OTP , $OTP_ID )
    {
        $user_email = $user->getEmail();
        if( GeneralUtility::validEmail( $user_email ) ){
            $subject = $this->translate('email_otp_subject');
            $senderEmail = $this->translate('email_otp_sender');
            $body = vsprintf( $this->translate('email_otp_template') , array('0'=>$OTP,'1'=> $OTP_ID));
            return $this->emailUtility->sendPlainMail( $user_email , $senderEmail, $subject, $body );
        }else{
            return false;
        }
    }

    /**
     * changepasswordAction
     * @return void
     */
    public function changepasswordAction()
    {
        //to get userid and forgothash value
        if( $this->request->hasArgument('user') && $this->request->hasArgument('forgothash') ) {
            //Check for the user
            $minLength = (int)$this->settings['newPasswordMinLength'] ?: 6;
            $uid = $this->request->getArgument('user');
            $piHash = $this->request->getArgument('forgothash');
            $hash = explode('|', $piHash);
            // User is Invalid or not with the link Hash
            if ((int)$uid === 0) {
                $msgArray['STATUS_MESSAGE'] = $this->cObj->stdWrap(
                    $this->translate('change_password_notvalid_message'),
                    $this->settings['forgotResetMessageEmailSentMessage_stdWrap.']
                );
            }else {
                $res = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
                    '*',
                    'fe_users',
                    'uid=' . (int)$uid 
                );
                if ($GLOBALS['TYPO3_DB']->sql_num_rows($res)) {
                    $user = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res);
                }

                $userHash = $user['felogin_forgotHash'];
                $compareHash = explode('|', $userHash);
                if (!$compareHash || !$compareHash[1] || $compareHash[0] < time() || $hash[0] != $compareHash[0] || md5($hash[1]) != $compareHash[1]) {
                $msgArray['STATUS_MESSAGE'] = $this->cObj->stdWrap(
                        $this->translate('change_password_notvalid_message'),
                        $this->settings['forgotResetMessageEmailSentMessage_stdWrap.']
                    );
                }   
            }
            $this->view->assign('changepassword',true);
            $this->view->assign('uid',$uid);
        } else { // All is fine, continue with new password
            $postData = GeneralUtility::_POST($this->prefixId);
            $minLength = (int)$this->settings['newPasswordMinLength'] ?: 6;
            if (isset($postData['changepasswordsubmit'])) {
                if (strlen($postData['password1']) < $minLength) {
                    $msgArray = $GLOBALS['TSFE']->cObj->stdWrap(
                    $this->translate('change_password_tooshort_message'),
                    $this->settings['forgotResetMessageEmailSentMessage_stdWrap.']
                );
            } elseif ($postData['password1'] != $postData['password2']) {
                $msgArray = $this->cObj->stdWrap(
                    $this->translate('change_password_notequal_message'),
                    $this->settings['forgotResetMessageEmailSentMessage_stdWrap.']
                );
            } else {
                //applying salting utility
                $newPass = $postData['password1'];
                $saltedPassword = ''; 
                if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('saltedpasswords')) { 
                    if (\TYPO3\CMS\Saltedpasswords\Utility\SaltedPasswordsUtility::isUsageEnabled('FE')) { 
                        $objSalt = \TYPO3\CMS\Saltedpasswords\Salt\SaltFactory::getSaltingInstance(NULL); 
                        if (is_object($objSalt)) { 
                            $saltedPassword = $objSalt->getHashedPassword($newPass); 
                        }
                    }
                }
                // Save new password and clear DB-hash
                $userid = $postData['userid'];
                $res = $GLOBALS['TYPO3_DB']->exec_UPDATEquery(
                    'fe_users',
                    'uid=' . $userid,
                    array('password' => $saltedPassword,
                        'felogin_forgotHash' => '', 
                        'tstamp' => $GLOBALS['EXEC_TIME']
                    )
                );
                if($res){
                    $msgsuccess = $GLOBALS['TSFE']->cObj->stdWrap(
                        $this->translate('change_password_done_message'),
                        $this->settings['forgotResetMessageEmailSentMessage_stdWrap.']
                    );
                    $this->view->assign('msgsuccess',$msgsuccess);
                }
            }
            if($msgArray) {
                $postData = GeneralUtility::_POST($this->prefixId);
                $this->view->assign('changepassword',true);
                $this->view->assign('msgArray',$msgArray);
                $this->view->assign('uid',$postData['userid']);
            }
            }    
        }
    }
}