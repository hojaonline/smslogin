<?php
namespace PITS\Smslogin\Services;

/**
 * Class Session Handler
 * @author HOJA.M.A <hoja.ma@pitsolutions.com>
 * Created by PhpStorm.
 * User: hoja
 * Date: 26/6/16
 * Time: 1:03 PM
 */
class SessionHandler implements \TYPO3\CMS\Core\SingletonInterface
{
    /**
     * prefixKey
     * @var string
     */
    private $prefixKey = 'tx_smslogin_smsfelogin_';

    /**
     * Returns the object stored in the userÂ´s PHP session
     * @return Object the stored object
     */
    public function restoreFromSession($key) 
    {
        $sessionData = $GLOBALS['TSFE']->fe_user->getKey('ses', $this->prefixKey . $key);
        return unserialize($sessionData);
    }

    /**
     * Writes an object into the PHP session
     * @param    $object any serializable object to store into the session
     * @return   WOI\WoiTrendingactivities\Service\SessionHandler this
     */
    public function writeToSession( $key , $object ) 
    {
        $sessionData = serialize($object);
        $GLOBALS['TSFE']->fe_user->setKey('ses', $this->prefixKey . $key, $sessionData);
        $GLOBALS['TSFE']->fe_user->storeSessionData();
        return $this;
    }

    /**
     * Cleans up the session: removes the stored object from the PHP session
     * @return   WOI\WoiTrendingactivities\Service\SessionHandler this
     */
    public function cleanUpSession($key) 
    {
        $GLOBALS['TSFE']->fe_user->setKey('ses', $this->prefixKey . $key, NULL);
        $GLOBALS['TSFE']->fe_user->storeSessionData();
        return $this;
    }

    /**
     * setterOverride for Prefix Key
     * @param key $prefixKey string
     */
    public function setPrefixKey($prefixKey) 
    {
        $this->prefixKey = $prefixKey;
    }
}