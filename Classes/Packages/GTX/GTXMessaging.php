<?php
namespace PITS\Smslogin\Packages\GTX;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 HOJA.M.A <hoja.ma@pitsolutions.com>, PIT Solutions PVT LTD
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
/**
 * Created by PhpStorm.
 * User: hoja
 * Date: 19/7/16
 * Time: 1:17 AM
 */
class GTXMessaging
{
    /**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     * @inject
     */
    protected $objectManager;

    /**
     * @param \TYPO3\CMS\Extbase\Object\ObjectManager $objectManager
     */
    public function injectObjectManager( \TYPO3\CMS\Extbase\Object\ObjectManager $objectManager ) 
    {
        $this->objectManager = $objectManager;
    }

    /**
     * gatewayURI
     * @var string
     */
    protected $gatewayURI = "http://http.gtx-messaging.net/smsc.php";

    /**
     * gatewayAPIkey
     * @var string
     */
    protected $gatewayAPIkey;

    /**
     * gatewayAPIpassword
     * @var string
     */
    protected $gatewayAPIpassword;


    /**
     * GTXMessaging constructor.
     * @param $configuration
     */
    public function __construct($configuration )
    {
        $api_key = isset( $configuration['smsloginAPI.']['key'] )?$configuration['smsloginAPI.']['key']:NULL;
        $api_password = isset( $configuration['smsloginAPI.']['key'] )?$configuration['smsloginAPI.']['secret']:NULL;
        $this->gatewayAPIkey = $api_key;
        $this->gatewayAPIpassword = $api_password;
    }

    /**
     * @param $from
     * @param $to
     * @param $message
     * @return array
     */
    public function sendSMS($from , $to , $message )
    {
        if ( empty($this->gatewayAPIkey) || empty($this->gatewayAPIpassword) ){
            $message = "API Not Configured Properly";
            $status = false;
        } else {
            $request_url = $this->gatewayURI."?user=".$this->gatewayAPIkey;
            $request_url .= "&pass=".$this->gatewayAPIpassword;
            $request_url .= "&from=".urlencode( $from );
            $request_url .= "&to=".urlencode( $to );
            $request_url .= "&text=".urlencode( $message );
            $environment = "PRODUCTION";
            if ( $environment == "DEVELOPMENT" ){
                echo $request_url;
                echo __FILE__.__LINE__."SMS SENDING DISABLED!!! PRODUCTION PURPOSE";
                return $response = array(
                        'status' => true,
                        'message' => "success!!!"
                );
            }
            $cp = curl_init($request_url);
            curl_setopt($cp, CURLOPT_HEADER, false);
            curl_setopt($cp, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($cp);
            if ($result === false) { // error handling 
                $message = "error not processed";
                $status = false;
            } else { // parse response message 
                $message = $result . " message send to: " . $to . $request_url . " send at:" . date("Y-m-d H:i:s");
                $status = true;
            }
            curl_close($cp);
        }
         
        $response = array( 
            'status' => $status,
            'message' => $message
        );
        return $response;
    }

    /**
     * @param $receiverEmail
     * @param $senderEmail
     * @param $subject
     * @param $body
     * @return bool
     */
    public function sendOTPEmail($receiverEmail, $senderEmail, $subject, $body )
    {
        /** @var \TYPO3\CMS\Core\Mail\MailMessage $message */
        $message = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Mail\\MailMessage');
        $message->setTo(array($receiverEmail => ''));
        $message->setFrom(array($senderEmail => 'Sender'));
        $message->setSubject($subject);
        $message->setBody($body);
        $message->send();
        return $message->isSent();
    }

    /**
     * Generate the authentication code
     *
     * @return void
     */
    public function generateOTP() 
    {
        mt_srand( time() );
        return mt_rand( 1000, 9999 );
    }
    /**
     * Generate the authentication code ID
     *
     * @return void
     */
    public function generateOTP_ID() 
    {
        mt_srand( time() );
        return mt_rand( 4589, 6587 );
    }
}