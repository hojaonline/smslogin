<?php
namespace PITS\Smslogin\Domain\Repository;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 HOJA.M.A <hoja.ma@pitsolutions.com>, PIT Solutions PVT LTD
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * The repository for FrontendUserRepository
 */
class FrontendUserRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

    // Example for repository wide settings
    public function initializeObject() 
    {
        /** @var $querySettings \TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings */
        $querySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
        // go for $defaultQuerySettings = $this->createQuery()->getQuerySettings(); if you want to make use of the TS persistence.storagePid with defaultQuerySettings(), see #51529 for details

        // don't add the pid constraint
        $querySettings->setRespectStoragePage(TRUE );
        // set the storagePids to respect
        $querySettings->setStoragePageIds(array(30));

        // define the enablecolumn fields to be ignored
        // if nothing else is given, all enableFields are ignored
        $querySettings->setIgnoreEnableFields(TRUE);
        // define single fields to be ignored
        $querySettings->setEnableFieldsToBeIgnored(array('disabled','starttime'));

        // add deleted rows to the result
        $querySettings->setIncludeDeleted(TRUE);

        // don't add sys_language_uid constraint
        $querySettings->setRespectSysLanguage(FALSE);

        $this->setDefaultQuerySettings($querySettings);
    }

    public function persistAll() 
    {
        $this->persistenceManager->persistAll();
        return true;
    }

    public function findAllUsers( $pid ) 
    {
        $querySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
        $querySettings->setRespectStoragePage(TRUE );
        $querySettings->setStoragePageIds(array($pid));
        $this->setDefaultQuerySettings($querySettings);

        $query = $this->createQuery();
        return $query->execute();
    }

    public function findOneByUsernameEntered( $username , $pid , $userGroupSelected = FALSE ) 
    {
        $querySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
        $querySettings->setRespectStoragePage(TRUE );
        $querySettings->setStoragePageIds(array($pid));
        $this->setDefaultQuerySettings($querySettings);
        if ( $this->findOneByUsername( $username ) instanceof \PITS\Smslogin\Domain\Model\FrontendUser ){
            $fe_user = $this->findOneByUsername( $username );
        } else if ( $this->findOneByEmail( $username ) instanceof \PITS\Smslogin\Domain\Model\FrontendUser ) {
            $fe_user = $this->findOneByEmail( $username );
        } else if ( $this->findOneByTelephone( $username ) instanceof \PITS\Smslogin\Domain\Model\FrontendUser ) {
            $fe_user = $this->findOneByEmail( $username );
        } else {
            $fe_user = false;
        }
        //Check Frontend Group
        if ( $fe_user instanceof \PITS\Smslogin\Domain\Model\FrontendUser ){
            $groups = $fe_user->getUsergroup();
            $groups_array = NULL;
            foreach ( $groups as $key => $group ){
                if ( $group instanceof \TYPO3\CMS\Extbase\Domain\Model\FrontendUserGroup ){
                    $groups_array[] = $group->getUid();
                }
            }
            if (!$userGroupSelected ){
                return $fe_user;
            }
            $groups_selected = explode(',',$userGroupSelected);
            $result = ( !empty(array_intersect($groups_array, $groups_selected)) )?$fe_user:FALSE;
            //DebuggerUtility::var_dump( $groups_array );
            //DebuggerUtility::var_dump( $groups_selected );
            //DebuggerUtility::var_dump($result);
            //exit;
            return $result;
        } else {
            return false;
        }
    }

    public function checkPassword( $password , $user_password_hash )
    {
        if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('saltedpasswords') && \TYPO3\CMS\Saltedpasswords\Utility\SaltedPasswordsUtility::isUsageEnabled('FE') ) {
            $saltedpasswordsInstance = \TYPO3\CMS\Saltedpasswords\Salt\SaltFactory::getSaltingInstance();
            $response = $saltedpasswordsInstance->checkPassword( $password, $user_password_hash );
            return $response;
        } else {
            return false;
        }
    }
}