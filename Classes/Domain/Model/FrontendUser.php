<?php
namespace PITS\Smslogin\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 HOJA.M.A <hoja.ma@pitsolutions.com>, PIT Solutions PVT LTD
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * FrontendUser
 */
class FrontendUser extends \TYPO3\CMS\Extbase\Domain\Model\FrontendUser
{
    /**
     * FrontendUser constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * otptoken
     * @var string
     */
    protected $tx_smslogin_otptoken = '';

    /**
     * otpstatus
     * @var string
     */
    protected $tx_smslogin_otpstatus = '';

    /**
     * @return string
     */
    public function getTxSmsloginOtptoken()
    {
        return $this->tx_smslogin_otptoken;
    }

    /**
     * @param string $tx_smslogin_otptoken
     */
    public function setTxSmsloginOtptoken($tx_smslogin_otptoken)
    {
        $this->tx_smslogin_otptoken = $tx_smslogin_otptoken;
    }

    /**
     * @return string
     */
    public function getTxSmsloginOtpstatus()
    {
        return $this->tx_smslogin_otpstatus;
    }

    /**
     * @param string $tx_smslogin_otpstatus
     */
    public function setTxSmsloginOtpstatus($tx_smslogin_otpstatus)
    {
        $this->tx_smslogin_otpstatus = $tx_smslogin_otpstatus;
    }

}